package com.zjiang91.weatherapp.ui

import android.app.Application
import com.zjiang91.weatherapp.ui.utils.DelegatesExt

/**
 * Created by zhoutsby on 1/31/16.
 */
class App: Application() {
    companion object {
        var instance: App by DelegatesExt.notNullSingleValue()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}