package com.zjiang91.weatherapp.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.zjiang91.weatherapp.R
import com.zjiang91.weatherapp.domain.commands.RequestForecastCommand
import com.zjiang91.weatherapp.ui.adapters.ForecastListAdapter
import org.jetbrains.anko.async
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import kotlinx.android.synthetic.activity_main.*

class MainActivity : AppCompatActivity() {

    @Override
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        forecastList.layoutManager = LinearLayoutManager(this)

        async() {
            val result = RequestForecastCommand("94085").execute()
            uiThread {
                forecastList.adapter = ForecastListAdapter(result) {toast(it.date)}
            }
        }
    }
}
