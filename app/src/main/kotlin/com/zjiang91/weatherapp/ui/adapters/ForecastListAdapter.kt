package com.zjiang91.weatherapp.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.zjiang91.weatherapp.R
import com.zjiang91.weatherapp.domain.model.Forecast
import com.zjiang91.weatherapp.domain.model.ForecastList
import com.zjiang91.weatherapp.ui.utils.ctx
import kotlinx.android.synthetic.item_forecast.view.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.onClick

/**
 * Created by zhoutsby on 1/30/16.
 */
class ForecastListAdapter(val weekForecast: ForecastList,
                                 val itemClick: (Forecast) -> Unit) :
        RecyclerView.Adapter<ForecastListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ViewHolder? {
        val view = parent.ctx.layoutInflater.inflate(R.layout.item_forecast, parent, false)
        return ViewHolder(view, itemClick);
    }

    override fun onBindViewHolder(holder: ViewHolder,
                                  position: Int) {
        holder.bindForecast(weekForecast[position])
    }

    override fun getItemCount() = weekForecast.size()

    class ViewHolder(val view: View, val itemClick: (Forecast) -> Unit): RecyclerView.ViewHolder(view) {

        fun bindForecast(forecast: Forecast) {
            with(forecast) {
                Picasso.with(itemView.ctx).load(iconUrl).into(itemView.icon)
                itemView.date.text = date
                itemView.description.text = description
                itemView.maxTemperature.text = "${high.toString()}"
                itemView.minTemperature.text = "${low.toString()}"
                itemView.onClick { itemClick(forecast) }
            }
        }
    }
}