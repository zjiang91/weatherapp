package com.zjiang91.weatherapp.ui.utils

import android.content.Context
import android.view.View

/**
 * Created by zhoutsby on 1/31/16.
 */
val View.ctx: Context
    get() = context