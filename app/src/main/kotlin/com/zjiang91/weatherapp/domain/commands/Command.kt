package com.zjiang91.weatherapp.domain.commands

/**
 * Created by zhoutsby on 1/30/16.
 */
interface Command<T> {
    fun execute(): T
}