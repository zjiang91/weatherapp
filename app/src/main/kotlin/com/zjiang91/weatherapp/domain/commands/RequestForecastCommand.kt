package com.zjiang91.weatherapp.domain.commands

import com.zjiang91.weatherapp.data.ForecastRequest
import com.zjiang91.weatherapp.domain.mappers.ForecastDataMapper
import com.zjiang91.weatherapp.domain.model.ForecastList

/**
 * Created by zhoutsby on 1/30/16.
 */
class RequestForecastCommand(private val zipCode: String):
        Command<ForecastList> {
    override fun execute(): ForecastList {
        val forecastResult = ForecastRequest(zipCode)
        return ForecastDataMapper().convertFromDataModel(
                forecastResult.execute()
        )
    }
}